package src;
import java.io.File;
import java.util.HashMap;
import java.util.List;



public class WordCount{

    public static void main(String[] args) {

        File file;
        FileRead fileRead = new FileRead();
        String data;

        //文本读入
        if(args.length > 0){
            file = new File(args[0]);
            data = fileRead.Input(file);
        }
        else{
            System.out.println("默认读入：input.txt 文本");
            file = new File("input.txt");
            data = fileRead.Input(file);
        }

        
        
        //处理文本
        Lib lib = new Lib();
        String dataLen = data.replaceAll("\r\n","\n");
        int length = dataLen.length();
        int wordAmount = lib.WordCount(data);
        int lines = lib.LineCount(data);
        List<HashMap.Entry<String, Integer>> wordList = lib.WordSort();

        //文本输出
        fileRead.Output(length,wordAmount,lines,wordList);


    }
}
